<?php

/**
 * @file
 * Install, update, and uninstall functions for the Domain Menus module.
 */

use Drupal\domain_menus\DomainMenusConstants;

/**
 * Implements hook_install().
 *
 * Sets module weight higher because hook alters should run after menu_ui.
 *
 * If domain_menus module was previously enabled and uninstalled, there may
 * be domain menus that had its domain menus third party settings removed
 * as a result of module uninstallation. Add domain menus third party
 * settings back to these menus on module install so the menus are recognized
 * as domain menus.
 */
function domain_menus_install() {
  module_set_weight('domain_menus', 1);

  $domains = \Drupal::entityTypeManager()->getStorage('domain')->loadMultiple();
  $menus = \Drupal::entityTypeManager()->getStorage('menu')->loadMultiple();
  foreach ($menus as $menu) {
    foreach ($domains as $domain) {
      $domain_id = $domain->id();
      $domain_domainid = $domain->getDomainId();
      $menu_name = '';
      $menu_domainid_partial = sprintf(DomainMenusConstants::DOMAIN_MENUS_MENU_ID_PATTERN, $domain_domainid, $menu_name);
      $menu_id = $menu->id();
      if (strpos($menu_id, $menu_domainid_partial) === 0) {
        $menu->setThirdPartySetting('domain_menus', 'domains', [$domain_id => $domain_id]);
        $menu->setThirdPartySetting('domain_menus', 'auto-created', 1);
        $menu->save();
      }
    }
    reset($domains);
  }
}

/**
 * Apply htmlspecialchars_decode() to domain menu labels.
 *
 * Versions prior to 8.x-1.0-alpha6 saved menu labels with special char encoding
 * and causes undesired output.
 */
function domain_menus_update_8101() {
  $menus = \Drupal::entityTypeManager()->getStorage('menu')->loadMultiple();
  foreach ($menus as $menu) {
    $menu_domains = $menu->getThirdPartySetting("domain_menus", "domains");
    if ($menu_domains !== NULL) {
      $menu->set('label', htmlspecialchars_decode($menu->get('label'), ENT_QUOTES));
      $menu->save();
    }
  }
}

/**
 * The weight of the domain_menus module is set to be after the menu_ui module.
 */
function domain_menus_update_8301() {
  $modules = \Drupal::config('core.extension')->get('module');
  if (isset($modules['menu_ui'])) {
    $weight = $modules['menu_ui']['weight'] + 1;
    module_set_weight('domain_menus', $weight);
  }
}
