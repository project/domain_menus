<?php

namespace Drupal\domain_menus\Plugin\Block;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\domain_menus\DomainMenusConstants;
use Drupal\system\Plugin\Block\SystemMenuBlock;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that displays an active domain menu.
 *
 * @Block(
 *   id = "domain_menus_active_domain_menu_block",
 *   admin_label = @Translation("Domain menus active domain menu"),
 *   category = @Translation("Domain"),
 * )
 */
class DomainMenusMenuBlock extends SystemMenuBlock {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The domain negotiator service.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'domain_menus.settings';

  /**
   * An array of settings.
   *
   * @var string[]
   */
  public $settings = [];

  /**
   * {@inheritdoc}
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, $menu_tree, $menu_active_trail, ConfigFactoryInterface $config_factory, DomainNegotiatorInterface $domain_negotiator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $menu_tree, $menu_active_trail);
    $this->configFactory = $config_factory;
    $this->domainNegotiator = $domain_negotiator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree'),
      $container->get('menu.active_trail'),
      $container->get('config.factory'),
      $container->get('domain.negotiator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeId() {
    $menu_name = $this->getSetting('menu_name');
    $domain = $this->domainNegotiator->getActiveDomain();
    $domain_domainid = $domain->getDomainId();

    $id = sprintf(DomainMenusConstants::DOMAIN_MENUS_MENU_ID_PATTERN, $domain_domainid, $menu_name);

    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $options = ['' => ''];
    $domain_menus_menu_names = $this->configFactory->get(static::SETTINGS)->get('domain_menus_menu_names');
    if (!empty($domain_menus_menu_names)) {
      $menu_names = explode("\r\n", $domain_menus_menu_names);
      foreach ($menu_names as $menu_name) {
        if (!empty($menu_name)) {
          $options[$menu_name] = $menu_name;
        }
      }
    }

    $form['menu_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Domain menu name'),
      '#default_value' => $this->getSetting('menu_name'),
      '#options' => $options,
      '#description' => '',
      '#required' => TRUE,
    ];

    return parent::blockForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Process the block's submission handling if no errors occurred only.
    if (!$form_state->getErrors()) {
      foreach (array_keys($this->defaultConfiguration()) as $element) {
        $this->configuration[$element] = $form_state->getValue($element);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $defaults = parent::defaultConfiguration();
    $defaults['menu_name'] = '';
    return $defaults;
  }

  /**
   * Gets the configuration for the block, loading defaults if not set.
   *
   * @param string $key
   *   The setting key to retrieve, a string.
   *
   * @return string
   *   The setting value, a string.
   */
  public function getSetting($key) {
    if (isset($this->settings[$key])) {
      return $this->settings[$key];
    }
    $defaults = $this->defaultConfiguration();
    if (isset($this->configuration[$key])) {
      $this->settings[$key] = $this->configuration[$key];
    }
    else {
      $this->settings[$key] = $defaults[$key];
    }
    return $this->settings[$key];
  }

}
