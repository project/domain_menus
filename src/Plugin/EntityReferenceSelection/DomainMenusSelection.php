<?php

namespace Drupal\domain_menus\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\domain_access\DomainAccessManagerInterface;

/**
 * Domain Menus plugin implementation of the Entity Reference Selection plugin.
 *
 * Returns matching nodes with domain assignment to the specified domain.
 *
 * @EntityReferenceSelection(
 *   id = "domain_menus:node",
 *   label = @Translation("Domain Menus"),
 *   base_plugin_label = @Translation("Domain Menus"),
 *   entity_types = {"node"},
 *   group = "domain_menus",
 *   weight = 1,
 * )
 */
class DomainMenusSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    $query = parent::buildEntityQuery($match, $match_operator);
    $config = $this->getConfiguration();
    $menu_domains = $config['menu_domains'] ?? NULL;
    $domain_access_field = defined('DOMAIN_ACCESS_FIELD') ? DOMAIN_ACCESS_FIELD : DomainAccessManagerInterface::DOMAIN_ACCESS_FIELD;
    $domain_access_all_field = defined('DOMAIN_ACCESS_ALL_FIELD') ? DOMAIN_ACCESS_ALL_FIELD : DomainAccessManagerInterface::DOMAIN_ACCESS_ALL_FIELD;
    if (!empty($menu_domains)) {
      $group = $query->orConditionGroup()
        ->condition($domain_access_field, $menu_domains, 'IN')
        ->condition($domain_access_all_field, '1');
      $query->condition($group);
    }
    return $query;
  }
}
