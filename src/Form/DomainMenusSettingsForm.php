<?php

namespace Drupal\domain_menus\Form;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\domain_menus\DomainMenusConstants;
use Drupal\system\MenuInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class of DomainMenusSettingsForm.
 */
class DomainMenusSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'domain_menus.settings';

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'domain_menus_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['domain_menus_menu_names'] = [
      '#type' => 'textarea',
      '#rows' => 5,
      '#columns' => 40,
      '#title' => $this->t('Domain menus'),
      '#default_value' => $config->get('domain_menus_menu_names'),
      '#description' => $this->t('Enter unique menu names/identifiers (less than 10 characters each, alphanumeric only), one per line.'),
    ];

    $form['domain_menus_filter_node_autocomplete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Filter internal node link autocomplete results'),
      '#default_value' => $config->get('domain_menus_filter_node_autocomplete') ?? 0,
      '#description' => $this->t('Makes autocomplete show only same domain nodes. Prevents creating inaccessible relative path links.'),
    ];

    $form['domain_menus_hide_from_menu_list'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide auto-created domain menus from admin/structure/menu'),
      '#default_value' => $config->get('domain_menus_hide_from_menu_list') ?? 0,
      '#description' => $this->t('Clear cache required. Disable this if there are issues with the menus list.'),
    ];

    $form['domain_menus_hide_admin_toolbar_links'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide auto-created domain menus from Admin Toolbar menu dropdown'),
      '#default_value' => $config->get('domain_menus_hide_admin_toolbar_links') ?? 0,
      '#description' => $this->t('Clear cache required. Disable this if there are issues with Admin Toolbar module.'),
    ];

    $form['domain_menus_create'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Operation: Create menus'),
      '#default_value' => 0,
      '#description' => $this->t('Create domain menus (not already existent) for every domain.'),
    ];

    $form['domain_menus_delete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Operation: Delete menus'),
      '#default_value' => 0,
      '#description' => $this->t('Delete all domain menus and all links in them!'),
    ];

    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($node_types as $id => $node_type) {
      $node_type_options[$id] = $node_type->label();
    }
    asort($node_type_options);
    $form['domain_menus_content_types_available_menus'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enable domain menus as menu parent for new link in selected node forms'),
      '#default_value' => $config->get('domain_menus_content_types_available_menus') ?? [],
      '#options' => $node_type_options,
    ];

    $form['domain_menus_default_parent_menu_name'] = [
      '#type' => 'textfield',
      '#title' => 'Domain menu name for default menu parent',
      '#default_value' => $config->get('domain_menus_default_parent_menu_name') ?? '',
      '#description' => $this->t('Leave empty to not override default menu parent with active domain domain menu. To override, should be one of the domain menu names/identifiers.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns an array of settings keys.
   */
  public function settingsKeys() {
    return [
      'domain_menus_menu_names',
      'domain_menus_filter_node_autocomplete',
      'domain_menus_content_types_available_menus',
      'domain_menus_default_parent_menu_name',
      'domain_menus_hide_from_menu_list',
      'domain_menus_hide_admin_toolbar_links',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save config settings.
    $config = $this->config(static::SETTINGS);
    foreach ($this->settingsKeys() as $key) {
      $value = $form_state->getValue($key);
      if (is_string($value)) {
        $value = trim($value);
      }
      $config->set($key, $value);
    }
    $config->save();
    parent::submitForm($form, $form_state);

    // Ensure menu storage is accessible.
    try {
      $menu_storage = $this->entityTypeManager->getStorage('menu');
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      $this->logger('domain_menus')->error($e->getMessage());
      return;
    }

    // Do bulk delete menus.
    if ($form_state->getValue('domain_menus_delete')) {
      $menus = $menu_storage->loadMultiple();
      foreach ($menus as $menu) {
        $menu_auto_created = $menu->getThirdPartySetting("domain_menus", "auto-created");
        if (!empty($menu_auto_created)) {
          $menu->delete();
        }
      }
      $this->messenger()->addMessage($this->t('Domain menu(s) deleted.'), 'status');
    }

    // Do bulk create menus.
    if ($form_state->getValue('domain_menus_create')) {
      $count = 0;
      $menu_names = explode("\r\n", $form_state->getValue('domain_menus_menu_names'));
      $domains = $this->entityTypeManager->getStorage('domain')->loadMultiple();
      foreach ($domains as $domain) {
        $domain_id = $domain->id();
        $domain_domainid = $domain->getDomainId();
        $domain_label = $domain->label();
        foreach ($menu_names as $menu_name) {
          if (!empty($menu_name)) {
            $menu_id = sprintf(DomainMenusConstants::DOMAIN_MENUS_MENU_ID_PATTERN, $domain_domainid, $menu_name);
            $menu_label = (string) $this->t('Domain menu for @domain_label (@menu_name)', [
              '@domain_label' => $domain_label,
              '@menu_name' => $menu_name,
            ]);
            $menu = $menu_storage->load($menu_id);
            if ($menu instanceof MenuInterface) {
              if (_domain_menus_is_auto_created($menu)) {
                // Skip creating existing autocreated menus.
                continue;
              }
            }
            else {
              $menu = $menu_storage->create([
                'id' => $menu_id,
                'label' => $menu_label,
              ]);
              $count++;
            }
            $menu->setThirdPartySetting('domain_menus', 'domains', [$domain_id => $domain_id]);
            $menu->setThirdPartySetting('domain_menus', 'auto-created', 1);
            $menu->save();
            if ($menu_id = $menu->id()) {
              domain_menus_copy_menu_translate_settings($menu_name, $menu_id);
            }
          }
        }
      }
      $this->messenger()->addMessage($this->t('@count domain menu(s) created.', ['@count' => $count]), 'status');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $domain_menus_menu_names = $form_state->getValue('domain_menus_menu_names');
    $menu_names = explode("\r\n", $domain_menus_menu_names);
    foreach ($menu_names as $menu_name) {
      if (!empty($menu_name) && (strlen($menu_name) > 9 || !ctype_alnum($menu_name))) {
        $form_state->setErrorByName('domain_menus_menu_names', $this->t('Domain menu names must be alphanumeric and less than 10 characters each.'));
      }
    }

    $domain_menus_default_parent_menu_name = $form_state->getValue('domain_menus_default_parent_menu_name');
    if (!empty($domain_menus_default_parent_menu_name) && !in_array($domain_menus_default_parent_menu_name, $menu_names)) {
      $form_state->setErrorByName('domain_menus_default_parent_menu_name', $this->t('Domain menu name for default menu parent should be one of the domain menu names/identifiers or empty.'));
    }
  }

}
