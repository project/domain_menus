<?php

namespace Drupal\domain_menus;

use Drupal\menu_ui\MenuListBuilder;

/**
 * Defines a class to build a listing of menus.
 */
class MenuNoDomainMenusListBuilder extends MenuListBuilder {

  /**
   * Loads entity IDs of menus excluding auto-created domain menus.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort($this->entityType->getKey('label'));

    // Remove auto-created domain menus.
    $menus = $this->getStorage()->loadMultiple();
    foreach ($menus as $id => $menu) {
      if (!empty($menu->getThirdPartySetting('domain_menus', 'auto-created'))) {
        unset($menus[$id]);
      }
    }
    if (!empty($menus)) {
      $query->condition('id', array_keys($menus), 'IN');
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query
        ->pager($this->limit);
    }
    return $query
      ->execute();
  }

}
