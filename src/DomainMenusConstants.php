<?php

namespace Drupal\domain_menus;

/**
 * Class for DomainMenusConstants.
 */
class DomainMenusConstants {

  const DOMAIN_MENUS_MENU_ID_PATTERN = 'dm%u-%s';
  const DOMAIN_MENUS_SETTINGS = 'domain_menus.settings';

}
