<?php

namespace Drupal\domain_menus;

use Drupal\menu_ui\MenuListBuilder;

/**
 * Defines a class to build a listing of domain menus.
 */
class DomainMenusListBuilder extends MenuListBuilder {

  /**
   * Loads entity IDs of domain menus.
   *
   * Loads entity IDs of domain menus limited by current user edit access,
   * using a pager sorted by the entity label.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort($this->entityType->getKey('label'));

    // Filter menus based on access.
    // Only return menus for which the user has 'edit' access.
    // We do this here to prevent paging issues (empty pages with pager).
    $menus = $this->getStorage()->loadMultiple();
    foreach ($menus as $id => $menu) {
      if (!$menu->access('edit') || empty($menu->getThirdPartySetting('domain_menus', 'domains'))) {
        unset($menus[$id]);
      }
    }
    if (!empty($menus)) {
      $query->condition('id', array_keys($menus), 'IN');
    }

    // If we don't find any menus, we return an empty result set.
    if (empty($menus)) {
      return [];
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query
        ->pager($this->limit);
    }
    return $query
      ->execute();
  }

}
