<?php

namespace Drupal\domain_menus\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\domain_menus\DomainMenusListBuilder;

/**
 * Class for DomainMenusController.
 */
class DomainMenusController extends ControllerBase {

  /**
   * List menus.
   */
  public function listMenus() {
    $entity_type = $this->entityTypeManager()->getDefinition('menu');
    $entity_storage = $this->entityTypeManager()->getStorage('menu');
    $menu_list = new DomainMenusListBuilder($entity_type, $entity_storage);
    return $menu_list->render();
  }

}
