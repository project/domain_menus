Allow menus to have domain assignment so that menu and 
menu link administration can be linked to domain access.

The module intention is to allow each domain to have its own set of menus like 
a main menu or footer menu to be administered by users with administrative 
permissions limited to specific domains.

Feature summary:
 * Menus can be assigned to domains
 * Configurable number of domain menu sets
 * Bulk create and delete option
 * Auto-create/auto-delete domain menus on domain create/delete
 * Active domain domain menus block

Installation:
Enable module as usual.
Then go to [admin/config/domain/domain_menus] to setup menu names and 
bulk create domain menus.
Checking "Create menus" and saving configuration will bulk create domain menus,
one for each menu name and for each domain.

Permissions:
Users with 'Administer all domain records' can access Domain Menus 
settings [admin/config/domain/domain_menus].

Users with 'Administer menus and menu options' permission can 
edit any domain menus.

After that, Domain Menus provides two role-assignable permissions:
 1. Edit assigned domain menus
 2. Edit active domain menus

These only have an effect if the domain_access module is enabled so users can
be assigned domains in the "Domain Access" field on the add/edit user form.

'Edit assigned domain menus' allows users to edit the domain menus of the
domains the user is assigned.

'Edit active domain menus' also requires intersecting user and menu domain
assignment but is further limited to the active domain.

The [admin/structure/domain-menus] page will display domain menus that the user
can edit.

Additional information:
Users with 'Administer menus and menu options' can freely alter domain 
assignment of menus on the add/edit menu page. However, this is not 
recommended unless there's a use-case for two or more domains and 
its admin users to have a common menu. The assumption is that
if there's a global menu it would use higher level menu 
permissions instead of domain permissions.

Warning:
Domain Menus creates menus with machine names like "dm[number]-[string]",
 eg. dm1234567-main. 
Avoid weird problems by not machine naming any other menus in such a pattern!
